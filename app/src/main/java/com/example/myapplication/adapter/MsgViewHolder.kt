package com.example.myapplication.adapter

import android.view.View
import android.widget.RadioButton
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.R

class MsgViewHolder(itemView:View): RecyclerView.ViewHolder(itemView) ,View.OnClickListener{
    val msg:TextView=itemView.findViewById(R.id.message)
    val radi:RadioButton=itemView.findViewById(R.id.radio)
    lateinit var clickedItem:ItemClickListener
    fun bind(msgText:String,selected:Boolean){
        msg.text=msgText
        radi.isChecked=selected
        radi.setOnClickListener {
                clickedItem.onItemClicked(null,layoutPosition)
        }
    }
    fun setItemClickListener(ic: ItemClickListener) {
        this.clickedItem = ic
    }


    override fun onClick(v: View?) {
        this.clickedItem.onItemClicked(v,getLayoutPosition());
    }
}


