package com.example.myapplication.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.R
import kotlin.properties.Delegates

class MessageAdapter(val msgArray:Array<String>): RecyclerView.Adapter<MsgViewHolder>(),ItemClickListener {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MsgViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.msg_item, parent, false)
        return MsgViewHolder(view)
    }
    var selectedPosition by Delegates.observable(0) { property, oldPos, newPos ->
        if (newPos in msgArray.indices) {
            notifyItemChanged(oldPos)
            notifyItemChanged(newPos)
        }
    }
    override fun getItemCount(): Int {
        return msgArray.size
    }

    override fun onBindViewHolder(holder: MsgViewHolder, position: Int) {
       holder.bind(msgArray[position],selectedPosition==position)

        holder.itemView.setOnClickListener {
            selectedPosition=position
        }
        holder.setItemClickListener(this)

    }

    override fun onItemClicked(view: View?, pos: Int) {
        selectedPosition=pos
    }
    fun getMessage():String{
        return msgArray[selectedPosition]
    }
}