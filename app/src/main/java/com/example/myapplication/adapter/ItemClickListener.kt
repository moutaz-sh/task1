package com.example.myapplication.adapter

import android.view.View

interface ItemClickListener {
    fun onItemClicked(view:View?,pos:Int)
}