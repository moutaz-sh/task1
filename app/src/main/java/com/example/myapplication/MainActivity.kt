package com.example.myapplication

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import android.widget.Button
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.adapter.MessageAdapter

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        var messageList=this.resources.getStringArray(R.array.message_array)
        val msgRecycler: RecyclerView = findViewById(R.id.question_container)
        val adapter=MessageAdapter(messageList)
        msgRecycler.adapter = adapter
        var message:Button=findViewById(R.id.message_btn)
        message.setOnClickListener(View.OnClickListener {
            Toast.makeText(baseContext, adapter.getMessage(), Toast.LENGTH_SHORT).show()
        })

    }
}