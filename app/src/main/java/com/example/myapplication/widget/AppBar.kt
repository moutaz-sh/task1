package com.example.myapplication.widget

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import com.example.myapplication.R
import de.hdodenhof.circleimageview.CircleImageView

class AppBar : ConstraintLayout {
   private var back_icon:CircleImageView
    get() = field
    private var star_icon:CircleImageView
    get() = field
    private var share_icon:CircleImageView
    get() = field
    private var root:View
    get() = field
     init{
        LayoutInflater.from(context).inflate(R.layout.app_bar,this);
        back_icon = findViewById(R.id.back_image);
        star_icon = findViewById(R.id.start_image);
        share_icon = findViewById(R.id.share_image);
        root = findViewById(R.id.root_view);
    }

    constructor(context: Context) : super(context){
    }
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs){
    }
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int, defStyleRes: Int) : super(context, attrs, defStyleAttr, defStyleRes)

}